package controller

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

// route
func HomeHandler(w http.ResponseWriter, r *http.Request) {
	// Reading request body
	// byteSlice := make([]byte, 999999)
	// _, myErr := r.Body.Read(byteSlice)
	// fmt.Println(string(byteSlice), myErr)

	// Writing response
	_, err := w.Write([]byte("hello world"))
	if err != nil {
		fmt.Println("error: ", err)
	}
}

func ParameterHandler(w http.ResponseWriter, r *http.Request) {
	// Accessing parameter
	p := mux.Vars(r)
	name := p["name"]

	_, err := w.Write([]byte("My name is " + name))
	if err != nil {
		fmt.Println("error: ", err)
		os.Exit(1)
	}
}
