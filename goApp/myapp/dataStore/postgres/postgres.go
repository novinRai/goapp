package postgres

import (
	"database/sql"
	"fmt"
	"log"

	// Drive for sql: indirect use
	_ "github.com/lib/pq"
)

// const keyword is used to determine
// const() similar to
// const postgres_host ...

// db details
const (
	postgres_host     = "dpg-chq2d4vdvk4goeoqkklg-a.singapore-postgres.render.com"
	postgres_port     = 5432
	postgres_user     = "postgres_admin"
	postgres_password = "BVCtK4KNI7zmECPbdPYtqM2jaUg58Q9e"
	postgres_dbname   = "my_db_5cur"
)

var Db *sql.DB

func init() {
	var err error
	// converting the data to be send into data base into a single string
	db_info := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s ", postgres_host, postgres_port, postgres_user, postgres_password, postgres_dbname)

	// Db is a postgres driver/handler
	Db, err = sql.Open("postgres", db_info)

	if err != nil {
		panic(err)
	} else {
		log.Println("Database Successfully configured")
	}
}
